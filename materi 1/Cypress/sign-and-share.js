/// <reference types="cypress" />

import 'cypress-file-upload'

describe ('empty test two', () =>{
    
    it('test two', ()=> {
      cy.visit('https://app.privy.id')
      
      
      cy.get('[name="user[privyId]"]').type('PF5984') //Input username
      cy.contains('CONTINUE').click() //Continue click

      cy.get('[name="user[secret]"]').type('Sayangpacar69') //Input Password
      cy.contains('CONTINUE').click() //Continue click

      cy.wait(2000)

      cy.get('[id="v-step-0"]').click() //Click icon share
      cy.contains('Self Sign').click() //Click Self Sign In
      cy.contains('Drag your document here or click browse').click()


        const documentFile = 'surat_jual.pdf' 
       
        cy.fixture('surat_jual.pdf').then(fileContent => {
          cy.get('input[type=file]').attachFile({ fileContent,fileName:documentFile, mimeType: 'application/pdf' })
        }) //Upload file

        cy.get('[class="modal-footer"]').contains('Upload').click()
        cy.wait(8000)
        cy.get('[id="step-document-1"]').click() 
        cy.wait(8000)

        cy.get('[id="step-document-1"]').click() //place

        cy.contains('Done').click()

        cy.contains('Send OTP').click()
        cy.wait(20000) // Menunggu otp via sms dan melakukan input manual
        cy.get('[class="modal-content"]').get('[form="form-sign"]').click({force: true}) //Sign In click

        cy.wait(4000)
        cy.get('[id="v-recipient-1"]').click()
        cy.get('[placeholder="Enter PrivyID"]').type('CO6456', {force: true})
        cy.wait(2000)
        cy.contains('CO6456').click()
        cy.wait(1000)
        cy.get('[type="radio"]').check({force: true})
        cy.wait(2000)
        cy.contains('- Select Role -').click()
        cy.wait(3000)
        cy.contains('Signer').click({force: true})
        cy.wait(2000)
        cy.contains('button', 'Add Recipient').click({force: true})
        cy.wait(2000)
        cy.contains('button', 'Continue').click()
        cy.wait(4000)
        cy.contains('button', 'Set Signature').click()
        cy.contains('Nicolas Ivan').click()
        cy.contains('Done').click()

    } )
} )
