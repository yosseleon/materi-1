/// <reference types="cypress" />

describe ('Self Sign In', () =>{
    it('test one', ()=> {
      cy.visit('https://app.privy.id')
      
      
      cy.get('[name="user[privyId]"]').type('PF5984') //Input username
      cy.contains('CONTINUE').click() //Continue click

      cy.get('[name="user[secret]"]').type('Sayangpacar69') //Input Password
      cy.contains('CONTINUE').click() //Continue click

      cy.wait(2000)

      cy.get('[id="v-step-0"]').click() //Click icon share
      cy.contains('Self Sign').click() //Click Self Sign In
      cy.contains('Drag your document here or click browse').click()
      
      const documentFile = 'surat_jual.pdf'  //file document dari fixtures
      cy.get('[type="file"]').attachFile({
          filePath: documentFile,
          encoding: 'base64'
      })// Upload file 

      cy.get('[class="modal-footer"]').contains('Upload').click() //Upload Click
      cy.wait(2000)
      cy.get('[id="step-document-1"]').click() //Add Signature
      cy.wait(2000)
      cy.get('[id="step-document-1"]').click() //Place
      cy.contains('Done').click() //Done Click
      
      cy.wait(2000)
      cy.contains('Send via SMS').click() 
      cy.contains('Send OTP').click() // Kirim OTP via sms
      cy.wait(20000) // Menunggu otp via sms dan melakukan input manual
      cy.get('[class="modal-content"]').get('[form="form-sign"]').click({force: true}) //Sign In click
    } )
} )
