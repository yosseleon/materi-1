/// <reference types="cypress" />

import 'cypress-file-upload'

describe ('empty test two', () =>{
    
    it('test two', ()=> {
        cy.visit('https://app.privy.id')
        
        cy.get('[name="user[privyId]"]').type('PF5984')

        cy.contains('CONTINUE').click()

        cy.get('[name="user[secret]"]').type('Sayangpacar69')

        cy.contains('CONTINUE').click()

        cy.contains('Change My Signature Image').click()

        cy.contains('Add Signature').click()

        cy.contains('Image').click()

        const documentPic = 'tandatangan.png' 
           
        cy.get('[class="d-flex cursor-pointer text-center text-primary box-height"]').contains('Browse').click()
       
        cy.fixture('tandatangan.png').then(fileContent => {
            cy.contains('Click to Upload Image').click().attachFile({ fileContent,fileName:documentPic, mimeType: 'png' })
          })
        // cy.contains('Click to Upload Image').click().attachFile(documentPic)
      })

    } )
